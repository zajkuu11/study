package staticMembers;

class Cat {

    // write static and instance variables
    private static int counter = 0;
    private String name;
    private int age;

    public static int getHEH(){
        return -1;
    }


    public Cat(String name, int age) {
        // implement the constructor
        // do not forgot to check the number of cats
        counter++;
        this.name = name;
        this.age = age;
        getHEH();
        if (getNumberOfCats() > 5)
            System.out.println("You have too many cats");

    }

    public static int getNumberOfCats() {
        // implement the static method
        return counter;
    }
}


class Time {

    int hours;
    int minutes;
    int seconds;

    public Time(int hours) {
        this.hours = hours;
    }

    public Time(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public Time(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
}


class Phone {

    String ownerName;
    String countryCode;
    String cityCode;
    String number;

    public Phone(String ownerName, String number) {
        this.ownerName = ownerName;
        this.number = number;
    }

    public Phone(String ownerName, String countryCode, String cityCode, String number) {
        this.ownerName = ownerName;
        this.countryCode = countryCode;
        this.cityCode = cityCode;
        this.number = number;
    }
}

class Movie {
    private String title;
    private String desc;
    private int year;

    // write two constructors here

    public Movie(String title, String desc, int year) {
        this.title = title;
        this.desc = desc;
        this.year = year;
    }

    public Movie(String title, int year) {
        this.title = title;
        this.year = year;
        this.desc = "empty";
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public int getYear() {
        return year;
    }
}