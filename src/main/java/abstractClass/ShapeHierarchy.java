package abstractClass;

class ShapeHierarchy {
    public static void main(String[] args) {
        Circle circle = new Circle(10);
        System.out.println(circle.getPerimeter());
        System.out.println(circle.getArea());
    }
}

abstract class Shape {

    abstract double getPerimeter();

    abstract double getArea();
}

class Rectangle extends Shape {
    double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    double getPerimeter() {
        return a * 2 + b * 2;
    }

    double getArea() {
        return a * b;
    }
}

class Triangle extends Shape {
    double a, b, c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }


    double getPerimeter() {
        return a + b + c;
    }

    double getArea() {
        double semiParameter = getPerimeter() / 2;
        return Math.sqrt(semiParameter * (semiParameter - a) * (semiParameter - b) * (semiParameter - c));
    }
}

class Circle extends Shape {
    double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    double getPerimeter() {
        return Math.PI * radius * 2;
    }

    double getArea() {
        return Math.PI * radius * radius;
    }
}