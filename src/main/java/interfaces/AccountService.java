package interfaces;

interface AccountService {
    Account findAccountByOwnerId(long id);
    long countAccountsWithBalanceGreaterThan(long value);


}



// Declare and implement your AccountServiceImpl here
class AccountServiceImpl implements AccountService {
    private Account[] accounts;

    public AccountServiceImpl(Account[] accounts) {
        this.accounts = accounts;
    }

    @Override
    public Account findAccountByOwnerId(long id) {
        Account foundAccount = null;
        for (Account account : accounts) {
            if (account.getOwner().getId() == id) {
                foundAccount = account;
            }
        }
        return foundAccount;
    }

    @Override
    public long countAccountsWithBalanceGreaterThan(long value) {
        long counter = 0;

        for (Account account : accounts) {
            if (account.getBalance() > value) {
                counter++;
            }
        }
        return counter;
    }

    public static void main(String[] args) {
        Account[] accounts = {
                new Account(1, 2000, new User(1, "heh", "tak")),
                new Account(2, 2000, new User(2, "heh", "tak")),
                new Account(3, 2000, new User(3, "heh", "tak")),
                new Account(4, 2000, new User(4, "heh", "tak"))
        };


        AccountServiceImpl accountService = new AccountServiceImpl(accounts);
        System.out.println(accountService.findAccountByOwnerId(2).getOwner().getId());
    }
}

class Account {

    private long id;
    private long balance;
    private User owner;

    public Account(long id, long balance, User owner) {
        this.id = id;
        this.balance = balance;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    public User getOwner() {
        return owner;
    }
}

class User {

    private long id;
    private String firstName;
    private String lastName;

    public User(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}