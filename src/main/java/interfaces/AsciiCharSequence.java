package interfaces;

public class AsciiCharSequence implements CharSequence {
    private byte[] bytes;

    public AsciiCharSequence(byte[] bytes) {
        this.bytes = bytes;
    }

    public int length() {
        return bytes.length;
    }

    public char charAt(int index) {
        return (char) bytes[index];
    }

    public CharSequence subSequence(int start, int end) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append((char) b);
        }
        return ((CharSequence) stringBuilder).subSequence(start, end);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append((char) b);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        CharSequence cs = new AsciiCharSequence(new byte[]{65, 66, 67});
        System.out.println(cs.toString());



    }

}