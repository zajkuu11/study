package anonymousClasses;

abstract class SuperClass {
    public static final int var = 10;

    public static void method1() {
    }

    public void method2() {
    }

    public abstract void method3();
}


public class NamesOfTheMethods {
    public static void main(String[] args) {
        SuperClass superClass = new SuperClass() {

            @Override
            public void method3() {
                System.out.println("method3");
            }

            @Override
            public void method2() {
                System.out.println("method2");
            }
        };

        superClass.method3();
        superClass.method2();
    }
}
